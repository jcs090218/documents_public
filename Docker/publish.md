First tag a image

```sh
$ docker tag <IMAGE> <REPO>:<TAG>
```

then push

```sh
$ docker push <REPO>:<TAG>
```
